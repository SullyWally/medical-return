﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Energy : MonoBehaviour
{
    AudioSource sfx;
    GameObject _patient;
    private Button _btn;
    [SerializeField] private int _amountEnergy = 1;
    [SerializeField] private float _timeout = 3f;


    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        _btn = gameObject.GetComponent<Button>();
        _patient = GameObject.Find("anatomy");
        _btn.onClick.AddListener(TaskOnClick);
        sfx = GameObject.Find("EnergySFX").GetComponent<AudioSource>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        _timeout -= Time.deltaTime;
        if (_timeout <= 0)
        {
            Destroy(gameObject);
        }
    }

    void TaskOnClick()
    {
        Debug.Log("pressed");
        _patient.GetComponent<Patient>().addEnergy(_amountEnergy);
        sfx.Play();
        Destroy(gameObject);
    }
}
