﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New BadHabit", menuName = "BadHabits")]
public class BadHabit : ScriptableObject
{
  [SerializeField] private string _name;
  [SerializeField] private float _damage;
  [SerializeField] private string _description;
  [SerializeField] private float _interval;
  [SerializeField] private float _startInverval;

  public string Name { get => _name; set => _name = value; }
  public float Damage { get => _damage; set => _damage = value; }
  public string Description { get => _description; set => _description = value; }
  public float Interval { get => _interval; set => _interval = value; }
  public float StartInverval { get => _startInverval; set => _startInverval = value; }
}
