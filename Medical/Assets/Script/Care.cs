﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Care", menuName = "Cares")]
public class Care : ScriptableObject {
    [SerializeField] private string _name;
    [SerializeField] private float _energy;
    [SerializeField] private float _energyPercentage;
    [SerializeField] private string _description;
    [SerializeField] private Sprite artWork;

    [SerializeField] private bool _onCooldown = false;
    [SerializeField] private float _cooldown = 10;
    [SerializeField] private float _defaultCooldown = 10;
    public string Name { get => _name; set => _name = value; }
    public float Energy { get => _energy; set => _energy = value; }
    public float EnergyPercentage { get => _energyPercentage; set => _energyPercentage = value; }
    public string Description { get => _description; set => _description = value; }
    public Sprite ArtWork { get => artWork; set => artWork = value; }
    public float Cooldown { get => _cooldown; set => _cooldown = value; }
    public bool OnCooldown { get => _onCooldown; set => _onCooldown = value; }
    public float DefaultCooldown { get => _defaultCooldown; set => _defaultCooldown = value; }

    public IEnumerator CooldownTimer()
    {
        OnCooldown = true;
        while(Cooldown > 0)
        {
            yield return new WaitForSeconds(1);
            Cooldown--;
        }
        Cooldown = 10;
        OnCooldown = false;
    }
}