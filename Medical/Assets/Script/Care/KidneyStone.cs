using UnityEngine;

public class KidneyStone : Care {
  /// <summary>
  /// Awake is called when the script instance is being loaded.
  /// </summary>
  void Awake()
  {
      
  }

  private void AllopurinoDrug()
  {

  }

  private void DiuretikDrug()
  {

  }

  private void AlfaDrug()
  {

  }

  private void LemonWater()
  {

  }

  private void PomegranateJuice()
  {

  }

  private void AppleVinegar()
  {

  }

  private void HighCalciumFood()
  {

  }

  private void DecreaseNabatiConsumtion()
  {

  }

  private void DecreaseSaltConsumtion()
  {

  }

  private void ShockWaveTherapy()
  {

  }

  private void KidneyStoneSurgery()
  {

  }

  private void Ureterolenoskopi()
  {
    
  }
}