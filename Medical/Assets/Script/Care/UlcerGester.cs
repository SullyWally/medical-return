using UnityEngine;

public class UlcerGester : Care {
  /// <summary>
  /// Awake is called when the script instance is being loaded.
  /// </summary>
  void Awake()
  {
      
  }

  private void AntibioticGastritisHelikobakterPilori()
  {

  }

  private void Antacids()
  {

  }

  private void AntagonistReceptorH2()
  {

  }

  private void Honey()
  {

  }

  private void AloeVera()
  {

  }

  private void LicoriceExtract()
  {

  }

  private void AvoidMilk()
  {

  }

  private void StopSmoking()
  {

  }

  private void FoodWithProbiotics()
  {

  }

  private void Surgery()
  {

  }

  private void PartialGrastectomyTherapy()
  {

  }

  private void AnastomosisBillrothTherapy()
  {
    
  }
}