using UnityEngine;

[CreateAssetMenu(fileName = "New Organ", menuName = "Organs")]
public class OrganManager : ScriptableObject {
  [SerializeField] private string _name;
  [SerializeField] private float _defaultHealth = 0;
  [SerializeField] private float _health = 0;
  [SerializeField] private float _maxHealth = 0;
  [SerializeField] private Sprite artWork = null;

  public string Name { get => _name; set => _name = value; }
  public float Health { get => _health; set => _health = value; }
  public float MaxHealth { get => _maxHealth; set => _maxHealth = value; }
  public Sprite ArtWork { get => artWork; set => artWork = value; }

  /// <summary>
  /// This function is called when the object becomes enabled and active.
  /// </summary>
  private void OnEnable()
  {
    _health = _defaultHealth;
    _maxHealth = 100;
  }

  public void Recovery(float amount)
  {
    float totalHealth = _health += amount;
    
    if (totalHealth >= 100)
      _health = 100;
    else
      _health = totalHealth;
  }
}