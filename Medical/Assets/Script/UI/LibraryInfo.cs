﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LibraryInfo : MonoBehaviour
{
    void HeartCoronerInfo(){
        SceneManager.LoadScene("HeartCoronerLib");
    }
    void CorPulmonaleInfo(){
        SceneManager.LoadScene("CorPulmonaleLib");
    }
    void FibrilasiInfo(){
        SceneManager.LoadScene("FibrilasiLib");
    }
    void BronchitisInfo(){
        SceneManager.LoadScene("BronchitisLib");
    }
    void PneumoniaInfo(){
        SceneManager.LoadScene("PneumonaiLib");
    }
    void LungCancerInfo(){
        SceneManager.LoadScene("LungCancerLib");
    }
    void KidneyStoneInfo(){
        SceneManager.LoadScene("KidneyStoneLib");
    }
    void KidneyFailureInfo(){
        SceneManager.LoadScene("KidneyFailLib");
    }
    void TyphusInfo(){
        SceneManager.LoadScene("TyphusLib");
    }
    void AppendixInfo(){
        SceneManager.LoadScene("AppendixLib");
    }
    void HepatitisAInfo(){
        SceneManager.LoadScene("HepatitisALib");
    }
    void HepatitisBInfo(){
        SceneManager.LoadScene("HepatitisBLib");
    }



}
