﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SFXOnOff : MonoBehaviour
{
    private MusicManager sfxOn;
    private static bool sfxOnOff = true;

    public Text textOn;

    // Start is called before the first frame update
    void Start()
    {
        sfxOn = GameObject.FindGameObjectWithTag("BGM").GetComponent<MusicManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (sfxOnOff)
        {
            textOn.text = "On";
        }
        else
        {
            textOn.text = "Off";
        }
    }

    public void triggerSFX()
    {
        if (sfxOn.SFX)
        {
            sfxOn.SFX = false;

            sfxOnOff = false;
        }
        else
        {
            sfxOn.SFX = true;

            sfxOnOff = true;

        }
    }
}
