﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMChange : MonoBehaviour
{

    private AudioSource BGM1;
    private AudioSource BGM2;
    private MusicManager m_Manager;

    private void Start() {
        m_Manager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        BGM1 = GameObject.Find("BGMMusic").GetComponent<AudioSource>();
        BGM2 = GameObject.Find("BGMMusic2").GetComponent<AudioSource>();

    }
    public void Change(){
        if(BGM1.isPlaying == true &&  m_Manager.BGM == true){
            BGM1.Stop();
            BGM2.Play();
        }
        else if(BGM2.isPlaying == true &&  m_Manager.BGM == true){
            BGM2.Stop();
            BGM1.Play();
        }
    }
}
