﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onOffMusic : MonoBehaviour
{
    private MusicManager m_Manager;
    private AudioSource BGM;

    private void Start()
    {
        m_Manager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        BGM = GameObject.Find("BGMMusic").GetComponent<AudioSource>();
    }

    public void onBGM(){
        if(m_Manager.BGM != true){
            BGM.Play();
            m_Manager.BGM = true;
        }
    }

    public void offBGM(){
        if(m_Manager.BGM != false){
            BGM.Stop();
            m_Manager.BGM = false;
        }
    }

    public void onSFX(){
        if(m_Manager.SFX != true){
            m_Manager.SFX = true;
        }
    }

    public void offSFX(){
        if(m_Manager.SFX != false){
            m_Manager.SFX = false;
        }
    }


}
