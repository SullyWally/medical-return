﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    public AudioSource SoundFX;

    private MusicManager m_Manager;

    void Start(){
        m_Manager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
    }

    // Start is called before the first frame update
    public void SFXPlay()
    {
        if(m_Manager.SFX == true){
            SoundFX.Play();
        }
    }

    
}
