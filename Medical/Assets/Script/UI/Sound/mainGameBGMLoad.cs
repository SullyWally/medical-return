﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainGameBGMLoad : MonoBehaviour
{
    public static mainGameBGMLoad mainGameBGM;
    private void Awake() {
        if(mainGameBGM == null){
            mainGameBGM = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if(mainGameBGM != this){
            Destroy(gameObject);
        }
    }
}
