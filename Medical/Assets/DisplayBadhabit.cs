using System.Collections;
using UnityEngine;

public class DisplayBadhabit : MonoBehaviour {
  [SerializeField] private BadHabit[] _badhabits;
  private Patient _patient;

  private void Awake()
  {
    _patient = GetComponent<Patient>();
  }

  private void Start()
  {
    foreach (BadHabit badhabit in _badhabits)
    {
      StartCoroutine(Badhabit(badhabit.Damage, badhabit.StartInverval, badhabit.Interval));
    }
  }

  private void Damage(float damage)
  {

      _patient.BadHabit(damage);
  }
  private IEnumerator Badhabit(float damage, float begin ,float repeatRate) {
     yield return new WaitForSeconds(begin);
     while(true) {
         Damage(damage);
         yield return new WaitForSeconds(repeatRate);
     }
  }
}