﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayDetailPanel : MonoBehaviour
{
    private Care _care;
    [SerializeField] private Text _title;
    [SerializeField] private Text _description;
    [SerializeField] private Image _artWork;
    [SerializeField] private Text _energy;
    [SerializeField] private Text _energyPercentage;
    [SerializeField] private Text _cooldownText;

    public Care Care { get => _care; set => _care = value; }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        _title.text = _care.Name;
        _description.text = _care.Description;
        _energy.text = _care.Energy.ToString();
        _energyPercentage.text = _care.EnergyPercentage.ToString() + " %";
        _artWork.sprite = _care.ArtWork;
        _cooldownText.text = _care.Cooldown.ToString() + " detik";
    }
}
