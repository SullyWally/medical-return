﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedicOption : MonoBehaviour
{
    private OrganManager _organ;

    [SerializeField] private Text _organName;
    [SerializeField] private GameObject _artWork;

    public OrganManager Organ { get => _organ; set => _organ = value; }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        _organName.text = _organ.Name;
        _artWork.GetComponent<SpriteRenderer>().sprite = Organ.ArtWork;
    }
    
}
